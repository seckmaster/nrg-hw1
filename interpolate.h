#ifndef HW1_INTERPOLATE_H
#define HW1_INTERPOLATE_H

#include <math.h>

float weight_basic(const Point3f& point,
                   const EmbellishedPoint<float>& known_point,
                   float p) {
    auto weight = 1.0f / powf(known_point.point.distance(point), p);
    return weight;
}

float interpolate_basic(const Point3f& point,
                        const std::vector<EmbellishedPoint<float>>& points,
                        float p) {
    auto weights_sum = 0.0f;
    auto weights_values_sum = 0.0f;
    for (auto point_it : points) {
        if (point_it.point == point) {
            return point_it.value;
        } else {
            auto weight = weight_basic(point, point_it, p);
            weights_sum += weight;
            weights_values_sum += weight * point_it.value;
        }
    }
    return weights_values_sum / weights_sum;
}

typedef std::vector<EmbellishedPoint<float>> InterpolationResult;

InterpolationResult basic_shepards_method(const std::vector<EmbellishedPoint<float>>& points,
                                          const Parameters& params) {
    auto step_x = (params.max_x - params.min_x) / (float) params.res_x;
    auto step_y = (params.max_y - params.min_y) / (float) params.res_y;
    auto step_z = (params.max_z - params.min_z) / (float) params.res_z;

    auto result = InterpolationResult();
    result.reserve(params.res_z * params.res_y * params.res_x);

    for (float z = params.min_z; z < params.max_z; z += step_z) {
        for (float y = params.min_y; y < params.max_y; y += step_y) {
            for (float x = params.min_x; x < params.max_x; x += step_x) {
                auto point = Point3f(x, y, z);
                auto interpolated_value = interpolate_basic(point, points, params.p);
                auto new_point = EmbellishedPoint(point, interpolated_value);
                result.push_back(new_point);
            }
        }
    }
    return result;
}

float weight_modified(const OctreePoint& point,
                      const OctreePoint& known_point,
                      float r) {
    auto distance = known_point.distance(point);
    auto weight = powf(fmax(0.0f, r - distance) / (r * distance), 2);
    return weight;
}

float interpolate_modified(const Bounds& bounds,
                           const Octree& octree) {
    auto points = octree.points_inside_bounds(bounds);
    auto weights_sum = 0.0f;
    auto weights_values_sum = 0.0f;
    for (auto point_it : points) {
        if (bounds.center == *point_it) {
            return point_it->value;
        } else {
            auto weight = weight_modified(bounds.center, *point_it, bounds.radius);
            weights_sum += weight;
            weights_values_sum += weight * point_it->value;
        }
    }
    if (weights_sum == 0) { return 0; }
    return weights_values_sum / weights_sum;
}

Octree construct_octree(std::vector<OctreePoint>&, uint, uint);

InterpolationResult modified_shepards_method(std::vector<OctreePoint>& points,
                                             Parameters params) {
    auto octree = construct_octree(points, 20, log2(points.size()) + 1);

    auto step_x = (params.max_x - params.min_x) / (float) params.res_x;
    auto step_y = (params.max_y - params.min_y) / (float) params.res_y;
    auto step_z = (params.max_z - params.min_z) / (float) params.res_z;

    auto result = InterpolationResult();
    result.reserve(params.res_z * params.res_y * params.res_x);

    Bounds bounds;
    bounds.radius = params.R;

    for (float z = params.min_z; z < params.max_z; z += step_z) {
        for (float y = params.min_y; y < params.max_y; y += step_y) {
            for (float x = params.min_x; x < params.max_x; x += step_x) {
                bounds.center = OctreePoint(x, y, z);

                auto interpolated_value = interpolate_modified(bounds, octree);
                auto new_point = EmbellishedPoint(x, y, z, interpolated_value);
                result.push_back(new_point);
            }
        }
    }
    return result;
}

Octree construct_octree(std::vector<OctreePoint>& points,
                        uint bucket_size,
                        uint maximum_depth) {
    auto tree = Octree();
    tree.build(points, bucket_size, maximum_depth);
    return tree;
}

#endif //HW1_INTERPOLATE_H
