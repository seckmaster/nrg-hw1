#ifndef HW1_UNITTEST_H
#define HW1_UNITTEST_H

#include <optional>

void unit_test_assert_true(bool);
void unit_test_assert_false(bool);
template<class T> void unit_test_assert_equal(T, T);
void unit_test_assert_equal(float, float);
void unit_test_assert_equal(double, double);
template<class T> void unit_test_assert_equal(const std::optional<T>&, const std::optional<T>&);
template<class T> void unit_test_assert_equal(const std::optional<T>&, T);
template<class T> void unit_test_assert_equal(T, const std::optional<T>&);
template<class T> void unit_test_assert_not_equal(T, T);
void unit_test_assert_not_equal(float, float);
void unit_test_assert_not_equal(double, double);
bool compare_float(float, float, float);
bool compare_double(double, double, double);

void unit_test_assert_true(bool res) {
    if (res) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << res << " is not equal to true!" << std::endl;
    }
}

void unit_test_assert_false(bool res) {
    if (!res) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << res << " is not equal to false!" << std::endl;
    }
}

template<class T>
void unit_test_assert_equal(T fst, T snd) {
    if (fst == snd) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << fst << " is not equal to " << snd << "!" << std::endl;
    }
}

void unit_test_assert_equal(float fst, float snd) {
    if (compare_float(fst, snd, 10e-6)) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << fst << " is not equal to " << snd << "!" << std::endl;
    }
}

void unit_test_assert_equal(double fst, double snd) {
    if (compare_double(fst, snd, 10e-6)) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << fst << " is not equal to " << snd << "!" << std::endl;
    }
}

template<class T> void unit_test_assert_equal(const std::optional<T>& fst, const std::optional<T>& snd) {
    if (fst && snd) {
        unit_test_assert_equal(*fst, *snd);
    } else if (!(fst.has_value() || snd.has_value())) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        if (fst) {
            std::cout << "unit_test_assert_equal failed, " << *fst << " is not equal to {}!" << std::endl;
        } else {
            std::cout << "unit_test_assert_equal failed, {} is not equal to " << *snd << "!" << std::endl;
        }
    }
}

template<class T> void unit_test_assert_equal(const std::optional<T>& fst, T snd) {
    if (fst) {
        unit_test_assert_equal(*fst, snd);
    } else {
        std::cout << "unit_test_assert_equal failed, {} is not equal to " << snd << "!" << std::endl;
    }
}

template<class T> void unit_test_assert_equal(T fst, const std::optional<T>& snd) {
    if (snd) {
        unit_test_assert_equal(fst, *snd);
    } else {
        std::cout << "unit_test_assert_equal failed, " << fst << " is not equal to {}!" << std::endl;
    }
}

template<class T> void unit_test_assert_not_equal(T fst, T snd) {
    if (fst != snd) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << fst << " is equal to " << snd << "!" << std::endl;
    }
}

void unit_test_assert_not_equal(float fst, float snd) {
    if (!compare_float(fst, snd, 10e-6)) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << fst << " is equal to " << snd << "!" << std::endl;
    }
}

void unit_test_assert_not_equal(double fst, double snd) {
    if (!compare_double(fst, snd, 10e-6)) {
        std::cout << "unit_test_assert_equal succeeded!" << std::endl;
    } else {
        std::cout << "unit_test_assert_equal failed, " << fst << " is equal to " << snd << "!" << std::endl;
    }
}

bool compare_float(float x, float y, float epsilon) {
    return fabs(x - y) < epsilon;
}

bool compare_double(double x, double y, double epsilon) {
    return fabs(x - y) < epsilon;
}
#endif //HW1_UNITTEST_H
