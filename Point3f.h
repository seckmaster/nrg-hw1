#ifndef HW1_POINT3F_H
#define HW1_POINT3F_H

struct Point3f {
    float x, y, z;

    Point3f() : x(0), y(0), z(0) {
    }

    Point3f(float x, float y, float z) : x(x), y(y), z(z) {
    }

    Point3f(const Point3f& point) : x(point.x), y(point.y), z(point.z) {
    }

    float distance(const Point3f& point) const {
        float diff_x = this->x - point.x;
        float diff_y = this->y - point.y;
        float diff_z = this->z - point.z;
        return sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z);
    }

    bool operator==(const Point3f& point) const {
        return compare_float(x, point.x, 10e-6) &&
               compare_float(y, point.y, 10e-6) &&
               compare_float(z, point.z, 10e-6);
    }

private:
    static bool compare_float(float x, float y, float epsilon) {
        return fabs(x - y) < epsilon;
    }
};

template<class T>
struct EmbellishedPoint {
    Point3f point;
    T value;

    EmbellishedPoint(const Point3f& point, T value) : point(point), value(value) {
    }

    EmbellishedPoint(float x, float y, float z, T value) : point(Point3f(x, y, z)), value(value) {
    }

    EmbellishedPoint(const EmbellishedPoint& point) : point(point.point), value(point.value) {
    }
};

#endif //HW1_POINT3F_H
