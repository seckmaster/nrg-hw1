//
// Created by Toni Kocjan on 03/03/2021.
//

#ifndef HW1_OCTREE_H
#define HW1_OCTREE_H

#include "Point3f.h"

class   Octree;
typedef bool            (*callback)(const Octree &o, void *data);

// -----------------------------------------------------------------------------
// This defines a point class (it's incomplete, but the data elements are there)
// -----------------------------------------------------------------------------

class OctreePoint {
public:

    float          x, y, z;        // Position
    float          n;              // User's unique identifier
    unsigned int    code;           // Used during octree generation
    float value = -1.0f;

    OctreePoint(float x, float y, float z, int n, unsigned int code) : x(x), y(y), z(z), n(n), code(code) {
    }

    OctreePoint(float x, float y, float z, int n) : x(x), y(y), z(z), n(n) {
    }

    OctreePoint(float x, float y, float z) : x(x), y(y), z(z) {
    }

    OctreePoint() : x(0), y(0), z(0) {}

    OctreePoint operator*(float scalar) const {
        return OctreePoint(x * scalar, y * scalar, z * scalar, n, code);
    }

    OctreePoint operator+(const OctreePoint& other) const {
        return OctreePoint(x + other.x, y + other.y, z + other.z, n, code);
    }

    OctreePoint operator-(const OctreePoint& other) const {
        return OctreePoint(x - other.x, y - other.y, z - other.z, n, code);
    }

    float distance(const Point3f& point) const {
        float diff_x = this->x - point.x;
        float diff_y = this->y - point.y;
        float diff_z = this->z - point.z;
        return sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z);
    }

    float distance(const OctreePoint& point) const {
        float diff_x = this->x - point.x;
        float diff_y = this->y - point.y;
        float diff_z = this->z - point.z;
        return sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z);
    }

    bool operator==(const Point3f& point) const {
        return compare_float(x, point.x, 10e-6) &&
               compare_float(y, point.y, 10e-6) &&
               compare_float(z, point.z, 10e-6);
    }

    bool operator==(const OctreePoint& point) const {
        return compare_float(x, point.x, 10e-6) &&
               compare_float(y, point.y, 10e-6) &&
               compare_float(z, point.z, 10e-6);
    }
private:
    static bool compare_float(float x, float y, float epsilon) {
        return fabs(x - y) < epsilon;
    }
};

// -----------------------------------------------------------------------------
// This defines a cubic bounding volume (center, radius)
// -----------------------------------------------------------------------------

struct Bounds {
    OctreePoint           center;         // Center of a cubic bounding volume
    float          radius;         // Radius of a cubic bounding volume

    Bounds() : center(OctreePoint(0, 0, 0)), radius(0) {
    }

    Bounds(float x, float y, float z, float radius) : center(OctreePoint(x, y, z)), radius(radius) {
    }

    bool intersects_with(const Bounds& other) const {
        auto distance = this->center.distance(other.center);
        return distance <= (this->radius + other.radius);
    }

    bool contains(const OctreePoint& point) const {
        return center.distance(point) <= radius;
    }
};

// -----------------------------------------------------------------------------
// The octree class -- almost real code!
// -----------------------------------------------------------------------------

class Octree {
public:
    // Construction/Destruction

    Octree();
    virtual                         ~Octree();

    // Accessors

    // Implementation

    virtual const bool build(std::vector<OctreePoint>& points,
                             const unsigned int threshold,
                             const unsigned int maximumDepth);

    std::vector<const OctreePoint*> points_inside_bounds(const Bounds& bounds) const;

protected:
    const bool build(std::vector<OctreePoint>& points,
                     const unsigned int bucket_size,
                     const unsigned int maximum_depth,
                     const Bounds &bounds,
                     const unsigned int currentDepth = 0);

    static const Bounds calcCubicBounds(const std::vector<OctreePoint>& points);

    void points_in_bounds(const Bounds& bounds, std::vector<const OctreePoint*>& accumulator) const;
protected:
    Octree *_child[8];
    unsigned int _pointCount;
    std::vector<OctreePoint> _points;
    Bounds bounds;
};

#endif //HW1_OCTREE_H
