// COTD Entry submitted by Paul Nettle [midnight@FluidStudios.com]
// Corresponds with an Ask MidNight response (http://www.flipcode.com/askmid/)

// -----------------------------------------------------------------------------
// This defines a callback for traversal
// -----------------------------------------------------------------------------

#include <iostream>
#include <vector>
#include "octree.h"

// -----------------------------------------------------------------------------
// Construction -- Just "nullify" the class
// -----------------------------------------------------------------------------

Octree::Octree() {
    memset(_child, 0, sizeof(_child));
}

// -----------------------------------------------------------------------------
// Destruction -- free up memory
// -----------------------------------------------------------------------------

Octree::~Octree() {
}

// -----------------------------------------------------------------------------
// Build the octree
// -----------------------------------------------------------------------------

const bool Octree::build(std::vector<OctreePoint>& points,
                         const unsigned int threshold,
                         const unsigned int maximumDepth) {
    const Bounds bounds = calcCubicBounds(points);
    return build(points, threshold, maximumDepth, bounds, 0);
}

const bool Octree::build(std::vector<OctreePoint>& points,
                         const unsigned int bucket_size,
                         const unsigned int maximum_depth,
                         const Bounds &bounds,
                         const unsigned int currentDepth) {
    // You know you're a leaf when...
    //
    // 1. The number of points is <= the bucket_size
    // 2. We've recursed too deep into the tree
    //    (currentDepth >= maximum_depth)
    //
    //    NOTE: We specifically use ">=" for the depth comparison so that we
    //          can set the maximum_depth depth to 0 if we want a tree with
    //          no depth.

    this->bounds = bounds;
    this->_pointCount = points.size();

    if (points.size() <= bucket_size || currentDepth >= maximum_depth) {
        // Just store the points in the node, making it a leaf
        this->_points.reserve(points.size());
        for (auto point : points) {
            _points.push_back(point);
        }
        return true;
    }

    // We'll need this (see comment further down in this source)

    unsigned int childPointCounts[8];
    for (int i = 0; i < 8; i++) { childPointCounts[i] = 0; }

    // Classify each point to a child node

    for (auto it = points.begin(); it != points.end(); it++) {
        // Current point

        OctreePoint &p = *it;

        // Center of this node

        const OctreePoint &c = bounds.center;

        // Here, we need to know which child each point belongs to. To
        // do this, we build an index into the _child[] array using the
        // relative position of the point to the center of the current
        // node

        p.code = 0;
        if (p.x > c.x) p.code |= 1;
        if (p.y > c.y) p.code |= 2;
        if (p.z > c.z) p.code |= 4;

        // We'll need to keep track of how many points get stuck in each
        // child so we'll just keep track of it here, since we have the
        // information handy.

        childPointCounts[p.code]++;
    }

    // Recursively call build() for each of the 8 children
    for (int i = 0; i < 8; i++) {
        // Don't bother going any further if there aren't any points for
        // this child
        if (!childPointCounts[i]) continue;

        auto tree = new Octree();
//        _child.insert(_child.begin() + i, tree);
        _child[i] = tree;

        // Allocate a list of points that were coded JUST for this child
        // only

        std::vector<OctreePoint> newList;
        newList.reserve(childPointCounts[i]);

        // Go through the input list of points and copy over the points
        // that were coded for this child

        for (auto it = points.begin(); it != points.end(); it++) {
            if (it->code == i) {
                newList.push_back(*it);
            }
        }

        // At this point, we have a list of points that will belong to
        // this child node. We'll want to remove any point with a
        // duplicate 'n' in them...
        //
        // [We won't need to reallocate the list, since it's temporary]

//        int newCount = 0;
//        for (int j = 0; j < childPointCounts[i]; j++)
//        {
//            // Remove duplicates from newList
//            // ...
//            // Keep track of the new count in 'newCount'
//        }
//        int newCount = childPointCounts[i];

        // Generate a new bounding volume -- We do this with a touch of
        // trickery...
        //
        // We use a table of offsets. These offsets determine where a
        // node is, relative to it's parent. So, for example, if want to
        // generate the bottom-left-rear (-x, -y, -z) child for a node,
        // we use (-1, -1, -1).
        //
        // However, since the radius of a child is always half of its
        // parent's, we use a table of 0.5, rather than 1.0.
        //
        // These values are stored the following table. Note that this
        // won't compile because it assumes Points are structs, but you
        // get the idea.

        static const OctreePoint boundsOffsetTable[8] =
                {
                        {-0.5, -0.5, -0.5},
                        {+0.5, -0.5, -0.5},
                        {-0.5, +0.5, -0.5},
                        {+0.5, +0.5, -0.5},
                        {-0.5, -0.5, +0.5},
                        {+0.5, -0.5, +0.5},
                        {-0.5, +0.5, +0.5},
                        {+0.5, +0.5, +0.5}
                };

        // Calculate our offset from the center of the parent's node to
        // the center of the child's node

        OctreePoint offset = boundsOffsetTable[i] * bounds.radius;

        // Create a new Bounds, with the center offset and half the
        // radius

        Bounds newBounds;
        newBounds.radius = bounds.radius * 0.5;
        newBounds.center = bounds.center + offset;

        // Recurse

        tree->build(newList, bucket_size, maximum_depth,
                    newBounds, currentDepth+1);

    }

    return true;
}

// -----------------------------------------------------------------------------
// Determine the [cubic] bounding volume for a set of points
// -----------------------------------------------------------------------------

const Bounds Octree::calcCubicBounds(const std::vector<OctreePoint>& points) {
    // What we'll give to the caller

    Bounds b;
    if (points.empty()) { return b; }

    // Determine min/max of the given set of points

    OctreePoint min = points[0];
    OctreePoint max = points[0];

    for (auto it = points.begin(); it != points.end(); it++) {
        const OctreePoint &p = *it;
        if (p.x < min.x) min.x = p.x;
        if (p.y < min.y) min.y = p.y;
        if (p.z < min.z) min.z = p.z;
        if (p.x > max.x) max.x = p.x;
        if (p.y > max.y) max.y = p.y;
        if (p.z > max.z) max.z = p.z;
    }

    // The radius of the volume (dimensions in each direction)

    OctreePoint radius = max - min;

    // Find the center of this space

    b.center = min + radius * 0.5;

    // We want a CUBIC space. By this, I mean we want a bounding cube, not
    // just a bounding box. We already have the center, we just need a
    // radius that contains the entire volume. To do this, we find the
    // maximum value of the radius' X/Y/Z components and use that

    b.radius = radius.x;
    if (b.radius < radius.y) b.radius = radius.y;
    if (b.radius < radius.z) b.radius = radius.z;

    // Done

    return b;
}

std::vector<const OctreePoint*> Octree::points_inside_bounds(const Bounds& bounds) const {
    std::vector<const OctreePoint*> points;
    points.reserve(_pointCount);
    points_in_bounds(bounds, points);
    return points;
}

void Octree::points_in_bounds(const Bounds& bounds,
                              std::vector<const OctreePoint*>& accumulator) const {
    if (!_points.empty()) {
        for (auto it = _points.begin(); it != _points.end(); it++) {
            if (it->distance(bounds.center) > bounds.radius) { continue; }
            accumulator.push_back(it.base());
        }
        return;
    }

    for (int i = 0; i < 8; i++) {
        if (_child[i] == nullptr) {
            continue;
        }
        if (!_child[i]->bounds.intersects_with(bounds)) {
            continue;
        }
        _child[i]->points_in_bounds(bounds, accumulator);
    }
}
