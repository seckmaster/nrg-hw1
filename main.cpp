#include <iostream>
#include <vector>
#include <math.h>
#include <sstream>
#include <functional>

#include "octree.h"
#include "unittest.h"
#include "arg_parser.h"
#include "interpolate.h"

template<class T>
std::vector<T> parse_points(const std::string& input,
                            const std::function<T(float,float,float,float)>& provider) {
    std::vector<T> points;
    std::string str = input;
    while (true) {
        float x, y, z, v;
        if (sscanf(str.c_str(), "%f %f %f %f", &x, &y, &z, &v) != 4) {
            return points;
        }
        points.push_back(provider(x, y, z, v));
        size_t pos;
        if ((pos = str.find('\n')) && pos < str.size() - 1) {
            str = str.substr(pos + 1);
            if (str.empty()) { break; }
        } else {
            break;
        }

    }
    return points;
}

std::vector<EmbellishedPoint<float>> parse_basic_points(const std::string& input) {
    return parse_points<EmbellishedPoint<float>>(input, [](float x, float y, float z, float value) {
        return EmbellishedPoint<float>(x,y,z,value);
    });
}

std::vector<OctreePoint> parse_octree_points(const std::string& input) {
    return parse_points<OctreePoint>(input, [](float x, float y, float z, float value) {
        auto point = OctreePoint(x,y,z);
        point.value = value;
        return point;
    });
}

std::string consume_istream(std::istream& stream) {
    std::stringstream input;
    std::string line;
    while (std::getline(stream, line)) {
        input << line << std::endl;
    }
    return input.str();
}

template<class T>
void dump(const InterpolationResult& result,
          std::ostream& stream,
          const std::function<T(float)>& convert) {
    for (auto it = result.begin(); it != result.end(); it++) {
        auto point = *it;
        auto mapped_value = convert(point.value);
        auto hex = reinterpret_cast<char *>(&mapped_value);
        stream.write(hex, sizeof mapped_value);
    }
}

void dump(const InterpolationResult& result,
          std::ostream& stream) {
    for (auto it = result.begin(); it != result.end(); it++) {
        auto point = *it;
        auto hex = reinterpret_cast<char *>(&point.value);
        stream.write(hex, sizeof point.value);
    }
}

void min_and_max(const InterpolationResult& result,
                 float& min, float& max) {
    assert(!result.empty());
    min = max = result[0].value;
    for (auto it = result.begin(); it != result.end(); it++) {
        if (it->value > max) {
            max = it->value;
        }
        if (it->value < min) {
            min = it->value;
        }
    }
}

void debug_vpt_uint8(const InterpolationResult& result,
                     const Parameters& params,
                     std::ostream& stream) {
    float min, max;
    min_and_max(result, min, max);
    float range = max - min;
    for (auto it = result.begin(); it != result.end(); it++) {
        auto clamped_f = 255.0f * (it->value - min) / range;
        auto clamped = (uint8_t) clamped_f;
        auto hex = reinterpret_cast<char *>(&clamped);
        stream.write(hex, sizeof clamped);
    }
}

// performs all unit tests
void test();

int main(int argc, char** argv) {
    std::stringstream ss;
    for (int i = 0; i < argc; i++) {
        ss << argv[i] << " ";
    }
    auto str = ss.str();
    auto optional_test = parse_bool(str, "--test");

    if (optional_test && optional_test.value()) {
        test();
    } else {
//        auto start = std::chrono::high_resolution_clock::now();

        auto input = consume_istream(std::cin);
        auto params = parse_parameters(str);

        if (params.method == BASIC) {
            auto points = parse_basic_points(input);
            auto result = basic_shepards_method(points, params);
            dump(result, std::cout);
//            debug_vpt_uint8(result, params, std::cout);
        } else {
            auto points = parse_octree_points(input);
            auto result = modified_shepards_method(points, params);
            dump(result, std::cout);
//            debug_vpt_uint8(result, params, std::cout);
        }

//        auto stop = std::chrono::high_resolution_clock::now();
//        auto duration = duration_cast<std::chrono::seconds>(stop - start);
//        std::cout << "Task completed in: " << duration.count() << "[s]" << std::endl;
    }
    return 0;
}

void test_argument_parser();
void test_point3f_distance();
void test_parse_points();
void test_bounds_intersect();

void test() {
    test_argument_parser();
    test_point3f_distance();
    test_parse_points();
    test_bounds_intersect();
}

void test_argument_parser() {
    std::cout << "test_argument_parser:" << std::endl;
    unit_test_assert_equal(std::string("string"), parse_arg("--method string", "--method"));
//    unit_test_assert_not_equal("string", parse_arg("--method str", "--method"));
    unit_test_assert_equal(10.123, parse_double("--double 10.123", "--double"));
    unit_test_assert_equal(10.123, parse_double("--other_arg true --double 10.123", "--double"));
    unit_test_assert_equal(10, parse_int("--int 10", "--int"));
    unit_test_assert_equal(10, parse_int("--other_arg true --int 10", "--int"));
    unit_test_assert_equal(std::optional<double>(), parse_double("--double string", "--double"));
    unit_test_assert_equal(std::optional<double>(), parse_double("--float 10.555", "--double"));
    unit_test_assert_equal<Method>(BASIC, parse_method("--method basic", "method"));
    unit_test_assert_equal<Method>(MODIFIED, parse_method("--method modified", "--method"));
    unit_test_assert_equal(std::optional<Method>(), parse_method("--method unknown", "--method"));
    unit_test_assert_equal(true, parse_bool("--bool 1", "--bool"));
    unit_test_assert_equal(true, parse_bool("--bool true", "--bool"));
    unit_test_assert_equal(false, parse_bool("--bool 0", "--bool"));
    unit_test_assert_equal(false, parse_bool("--bool false", "--bool"));
    unit_test_assert_equal(std::optional<bool>(), parse_bool("--bool 2", "--bool"));

    auto params = parse_parameters(
            "--method modified --r 0.5 --min-x -1.5 --min-y -1.5 "
            "--min-z -1 --max-x 1.5 --max-y 1.5 --max-z 1 "
            "--res-x 128 --res-y 128 --res-z 64");
    unit_test_assert_equal(MODIFIED, params.method);
    unit_test_assert_equal(0.5f, params.R);
    unit_test_assert_equal(-1.5f, params.min_x);
    unit_test_assert_equal(1.5f, params.max_x);
    unit_test_assert_equal(-1.5f, params.min_y);
    unit_test_assert_equal(1.5f, params.max_y);
    unit_test_assert_equal(-1.f, params.min_z);
    unit_test_assert_equal(1.f, params.max_z);
    unit_test_assert_equal(128, params.res_x);
    unit_test_assert_equal(128, params.res_y);
    unit_test_assert_equal(64, params.res_z);

    params = parse_parameters(
            "--method basic --p 1.2 --min-x -1.5 --min-y -1.5 "
            "--min-z -1 --max-x 1.5 --max-y 1.5 --max-z 1 "
            "--res-x 128 --res-y 128 --res-z 64");
    unit_test_assert_equal(BASIC, params.method);
    unit_test_assert_equal(1.2f, params.p);
}

void test_point3f_distance() {
    std::cout << "test_point3f_distance:" << std::endl;
    unit_test_assert_equal(0.0f, Point3f().distance(Point3f()));
    unit_test_assert_equal(sqrtf(3), Point3f(1, 1, 1).distance(Point3f(0, 0, 0)));
    unit_test_assert_equal(sqrtf(641), Point3f(2, 3, -5).distance(Point3f(-2, 3, 20)));
}

void test_parse_points() {
    std::cout << "test_parse_points:" << std::endl;
    unit_test_assert_equal<int>(0, parse_basic_points("").size());
    std::string input_string =
            "2.312 6.223 1.232 0.222\n"
            "-0.442 4.623 -2.233 3.659\n"
            "0.212 -1.776 0.781 5.845";
    auto points = parse_basic_points(input_string);
    unit_test_assert_equal<int>(3, points.size());
}

void test_bounds_intersect() {
    std::cout << "test_bounds_intersect:" << std::endl;
    unit_test_assert_false(Bounds(0,0,0, 10).intersects_with(Bounds(11,11,11, 1)));
    unit_test_assert_false(Bounds(0,0,0, 3).intersects_with(Bounds(-10, 4, 2, 4)));
    unit_test_assert_true(Bounds(0,0,0, 3).intersects_with(Bounds(0,0,0, 3)));
    unit_test_assert_true(Bounds(0,0,0, 1).intersects_with(Bounds(0,0,0, 3)));
    unit_test_assert_true(Bounds(0,0,0, 3).intersects_with(Bounds(0,0,0, 1)));
    unit_test_assert_true(Bounds(0,0,0, 1).intersects_with(Bounds(1,0,0, 1)));
    unit_test_assert_true(Bounds(0,0,0, 2).intersects_with(Bounds(3,3,3, 3.2)));
}
