cmake_minimum_required(VERSION 3.17)
project(HW1)

set(CMAKE_CXX_STANDARD 20)

include_directories(
)

add_executable(interpolate main.cpp octree.cpp octree.h octree.h unittest.h arg_parser.h interpolate.h Point3f.h)